# Backend configuration for saving and locking tfstate file
terraform {
  
  backend "s3" {
    bucket         = "terra-tif-s3-state-lock1"
    key            = "platform/terraform.tfstate"
    region         = "us-east-1"
    encrypt        = false
    dynamodb_table = "terra-ddb-state-lock"
  }
}